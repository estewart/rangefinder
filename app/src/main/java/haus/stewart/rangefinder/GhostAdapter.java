package haus.stewart.rangefinder;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class GhostAdapter extends RecyclerView.Adapter<GhostAdapter.GhostViewHolder>
{

    private final Activity context;
    private Ghost[] ghosts;
    private Listener listener;

    public GhostAdapter(Activity context, Ghost[] ghosts)
    {
        this.context = context;
        this.listener = (Listener) context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GhostViewHolder onCreateViewHolder(ViewGroup parent,
                                              int viewType)
    {
        if (ghosts == null)
        {
            ghosts = Ghost.GetAllGhosts();
        }
        if (listener == null)
        {
            this.listener = (Listener) context;
        }

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.fragment_ghost_item, null, true);

        GhostViewHolder viewHolder = new GhostViewHolder(rowView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(GhostViewHolder holder, int position)
    {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        LinearLayout rowView = holder.ghostView;
        TextView ghostName = (TextView) rowView.findViewById(R.id.ghostName);
        ImageView ghostIcon = (ImageView) rowView.findViewById(R.id.ghostIcon);
        TextView ghostHeight = (TextView) rowView.findViewById(R.id.knownHeight);
        TextView ghostWidth = (TextView) rowView.findViewById(R.id.knownWidth);
        ghostName.setText(ghosts[position].Name);
        ghostIcon.setImageResource(ghosts[position].ImgResourceId);
        ghostHeight.setText(Units.FeetDoubleToString(ghosts[position].ActualHeightInFeet));
        ghostWidth.setText(Units.FeetDoubleToString(ghosts[position].ActualWidthInFeet));

        rowView.setOnClickListener((View view) -> listener.ghostMenuItemClicked(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {

        if (ghosts == null)
        {
            ghosts = Ghost.GetAllGhosts();
        }
        return ghosts.length;
    }


    //listener interface communicates between fragment and activity
    interface Listener
    {
        //Any activities that use the Listener interface must implement ghostMenuItemClicked method
        void ghostMenuItemClicked(int position);
    }

    public static class GhostViewHolder extends RecyclerView.ViewHolder
    {
        // each data item is just a string in this case
        public LinearLayout ghostView;

        public GhostViewHolder(View v)
        {
            super(v);
            ghostView = (LinearLayout) v;
        }
    }


}
