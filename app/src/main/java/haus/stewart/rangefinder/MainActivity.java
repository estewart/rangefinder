package haus.stewart.rangefinder;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.Display;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraX;
import androidx.camera.core.Preview;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener, GhostAdapter.Listener
{

    // Constants
    private final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA"};
    private final int REQUEST_CODE_PERMISSIONS = 10; //arbitrary number, can be changed accordingly

    // Main UI components
    private TextureView _cameraView;
    private ImageView _ghostImageView;
    private TextView _baseHeightText;
    private TextView _distanceResultText;
    private ViewGroup _rootLayout;
    private RecyclerView _ghostList;
    private PopupMenu _settingsPopupMenu;
    private ScaleGestureDetector _scaleGestureDetector;

    // Calibration Mode UI Components
    private TextView _calibrationModeInstructions;
    private SeekBar _horizontalSeekBar;
    private SeekBar _verticalSeekBar;
    private Button _resetCalibrationButton;

    // Settings for the ghost image
    private Ghost _currentGhost;
    private double _defaultImageHeightInPixels;
    private double _defaultImageWidthInPixels;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Load saved state settings
        Settings.LoadEphemeralSettings(savedInstanceState);

        // Load persisted settings
        Settings.LoadPersistedSettings(this);

        // Initialize UI Components
        initializeLayout();

        // Set up Ghost Image
        prepareGhostImage();

        // Ensure we have permissions
        if (allPermissionsGranted())
        {
            startCamera(); //start camera if permission has been granted by user
        }
        else
        {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }

        // Display UI
        updateDistanceUI();
    }

    private double distanceToObject(double mScaleFactor)
    {
        return _currentGhost.KnownDistanceInFeet * ((_currentGhost.ScreenHeightAtDistanceInFeet) / (_currentGhost.ScreenHeightAtDistanceInFeet * mScaleFactor));
    }

    //region SavedInstanceState Handlers for Orientation Changes
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        Settings.SaveEphemeralSettings(savedInstanceState);
    }

    //endregion

    //region Main UI Component Code

    private void initializeLayout()
    {
        // UI Components
        setContentView(R.layout.activity_main);
        _rootLayout = findViewById(R.id.root);
        _cameraView = findViewById(R.id.view_finder);
        _distanceResultText = findViewById(R.id.distanceResult);
        _baseHeightText = findViewById(R.id.baseHeight);
        _ghostImageView = new ImageView(this);
        _ghostImageView.setId(View.generateViewId());
        _settingsPopupMenu = new PopupMenu(this, findViewById(R.id.settings_button));
        _settingsPopupMenu.setOnMenuItemClickListener(this);
        _settingsPopupMenu.inflate(R.menu.main_menu);
        _settingsPopupMenu.getMenu().getItem(1).getSubMenu().getItem(0).setChecked(true);

        _ghostList = findViewById(R.id.listView);
        GhostAdapter adapter = new GhostAdapter(this, Ghost.GetAllGhosts());
        _ghostList.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        _ghostList.setLayoutManager(layoutManager);
        _ghostList.addItemDecoration(new DividerItemDecoration(_ghostList.getContext(),
                DividerItemDecoration.HORIZONTAL));
        _ghostList.setVisibility(View.INVISIBLE);

        BottomAppBar bar = findViewById(R.id.bottom_app_bar);
        setSupportActionBar(bar);
        registerForContextMenu(bar);
        FloatingActionButton fab = findViewById(R.id.expand_ghosts_button);
        fab.setOnClickListener((View view) -> ToggleGhostPickerMenu());
        _scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
    }

    private void prepareGhostImage()
    {
        // Settings for the ghost image
        Ghost.LoadGhosts(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        hideCalibrationModeUI();

        if (Settings.CalibrationModeEnabled)
        {
            _currentGhost = Ghost.GetReferenceGhost(this);
            showCalibrationModeUI(metrics);
        }
        else
        {
            _currentGhost = Ghost.GetGhost(Settings.CurrentGhostName);
        }
        _baseHeightText.setText(Units.FeetDoubleToString(_currentGhost.ActualHeightInFeet));

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            // Position and scaling settings for the ghost image
            _defaultImageHeightInPixels = metrics.ydpi * (_currentGhost.ScreenHeightAtDistanceInFeet * 12);
            _defaultImageWidthInPixels = metrics.xdpi * (_currentGhost.ScreenWidthAtDistanceInFeet * 12);
        }
        else
        {
            // Position and scaling settings for the ghost image
            _defaultImageHeightInPixels = metrics.xdpi * (_currentGhost.ScreenHeightAtDistanceInFeet * 12);
            _defaultImageWidthInPixels = metrics.ydpi * (_currentGhost.ScreenWidthAtDistanceInFeet * 12);
        }


        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                (int) _defaultImageWidthInPixels, (int) _defaultImageHeightInPixels);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        _ghostImageView.setImageResource(_currentGhost.ImgResourceId);
        _ghostImageView.setColorFilter(Settings.GhostImageFilterColor);
        _ghostImageView.setMinimumHeight((int) _defaultImageHeightInPixels);
        _ghostImageView.setMinimumWidth((int) _defaultImageWidthInPixels);
        _ghostImageView.setLayoutParams(layoutParams);
        scaleGhostImage();

    }

    private void scaleGhostImage()
    {
        _ghostImageView.setScaleX(
                (float) Settings.GhostImageScaleFactor * Settings.GetXScaleFactor());
        _ghostImageView.setScaleY(
                (float) Settings.GhostImageScaleFactor * Settings.GetYScaleFactor());
    }

    private void updateDistanceUI()
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        _distanceResultText.setText(Units.FeetDoubleToString(distanceToObject(
                Settings.GhostImageScaleFactor)));
        _baseHeightText.setText(Units.FeetDoubleToString(_currentGhost.ActualHeightInFeet));
    }
    //endregion

    //region Calibration Mode UI

    private void showCalibrationModeUI(DisplayMetrics metrics)
    {
        _horizontalSeekBar = new SeekBar(this);
        RelativeLayout.LayoutParams xlayoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        xlayoutParams.setMargins(30, 30, 30, 100);
        xlayoutParams.width = 500;
        xlayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        xlayoutParams.addRule(RelativeLayout.ABOVE, _ghostImageView.getId());
        _horizontalSeekBar.setLayoutParams(xlayoutParams);
        _horizontalSeekBar.setProgress((int) (Settings.GetXScaleFactor() * 50));
        _horizontalSeekBar.setOnSeekBarChangeListener(new SeekBarListener());

        _verticalSeekBar = new VerticalSeekBar(this);
        RelativeLayout.LayoutParams ylayoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ylayoutParams.setMargins(30, 30, 100, 30);
        ylayoutParams.height = 500;
        ylayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        ylayoutParams.addRule(RelativeLayout.LEFT_OF, _ghostImageView.getId());
        _verticalSeekBar.setLayoutParams(ylayoutParams);
        _verticalSeekBar.setProgress((int) (Settings.GetYScaleFactor() * 50));
        _verticalSeekBar.setOnSeekBarChangeListener(new SeekBarListener());


        RelativeLayout.LayoutParams instructionLayoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams buttonLayoutParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            buttonLayoutParams.setMargins(30, 100, 30, 30);
            buttonLayoutParams.width = 400;
            buttonLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            buttonLayoutParams.addRule(RelativeLayout.BELOW, _ghostImageView.getId());

            instructionLayoutParams.setMargins(30, 30, 30, 250);
            instructionLayoutParams.width = 650;
            instructionLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            instructionLayoutParams.addRule(RelativeLayout.ABOVE, _ghostImageView.getId());
        }
        else
        {
            buttonLayoutParams.setMargins(100, 30, 30, 30);
            buttonLayoutParams.width = 400;
            buttonLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            buttonLayoutParams.addRule(RelativeLayout.RIGHT_OF, _ghostImageView.getId());

            instructionLayoutParams.setMargins(30, 30, 200, 30);
            instructionLayoutParams.width = 650;
            instructionLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            instructionLayoutParams.addRule(RelativeLayout.LEFT_OF, _ghostImageView.getId());
        }

        _resetCalibrationButton = new Button(this);
        _resetCalibrationButton.setLayoutParams(buttonLayoutParams);
        _resetCalibrationButton.setText("Reset to Default");
        _resetCalibrationButton.setOnClickListener(view ->
        {
            Settings.SetXScaleFactor(1f);
            Settings.SetYScaleFactor(1f);
            _horizontalSeekBar.setProgress((int) (Settings.GetXScaleFactor() * 50));
            _verticalSeekBar.setProgress((int) (Settings.GetYScaleFactor() * 50));
            scaleGhostImage();

        });

        _calibrationModeInstructions = new TextView(this);
        _calibrationModeInstructions.setLayoutParams(instructionLayoutParams);
        _calibrationModeInstructions.setText(
                "CALIBRATION MODE:\n\nUse a ruler to measure the actual height and width of this square on screen.\n\nScale the image with the sliders until it measures exactly 1 inch x 1 inch on your device screen");


        _cameraView.setVisibility(View.INVISIBLE);
        _rootLayout.addView(_resetCalibrationButton);
        _rootLayout.addView(_horizontalSeekBar);
        _rootLayout.addView(_verticalSeekBar);
        _rootLayout.addView(_calibrationModeInstructions);
    }

    private void hideCalibrationModeUI()
    {
        _rootLayout.removeView(_resetCalibrationButton);
        _rootLayout.removeView(_horizontalSeekBar);
        _rootLayout.removeView(_verticalSeekBar);
        _rootLayout.removeView(_calibrationModeInstructions);
        _cameraView.setVisibility(View.VISIBLE);

        _resetCalibrationButton = null;
        _horizontalSeekBar = null;
        _verticalSeekBar = null;
        _calibrationModeInstructions = null;
    }
    //endregion

    //region Menus and Menu Handlers
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.unit_switch_menu, menu);

        MenuItem unitSwitch = menu.findItem(R.id.unit_switch_setting);
        setUnitStringForMenu(unitSwitch);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId())
        {
            case R.id.unit_switch_setting:
                Settings.ToggleUseMetricUnits();
                updateDistanceUI();
                RefreshGhostPickerMenu();
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void showPopup(View v)
    {
        setUnitStringForMenu(_settingsPopupMenu.getMenu().findItem(R.id.unit_switch_setting));
        _settingsPopupMenu.getMenu().findItem(R.id.menu_calibrationMode).setChecked(
                Settings.CalibrationModeEnabled);
        int colorMenuIndexChecked = 0;
        switch (Settings.GhostImageFilterColor)
        {
            case Color.GREEN:
                colorMenuIndexChecked = 1;
                break;
            case Color.RED:
                colorMenuIndexChecked = 2;
                break;
        }
        _settingsPopupMenu.getMenu().getItem(1).getSubMenu().getItem(
                colorMenuIndexChecked).setChecked(true);
        _settingsPopupMenu.show();

    }

    private void setUnitStringForMenu(MenuItem item)
    {
        if (Settings.GetUseMetricUnits())
        {
            item.setTitle(R.string.menu_switchToImperial);
        }
        else
        {
            item.setTitle(R.string.menu_switchToMetric);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.unit_switch_setting:
                Settings.ToggleUseMetricUnits();
                updateDistanceUI();
                RefreshGhostPickerMenu();
                return true;
            case R.id.ghostColor_default:
                Settings.GhostImageFilterColor = Color.BLACK;
                item.setChecked(true);
                break;
            case R.id.ghostColor_green:
                Settings.GhostImageFilterColor = Color.GREEN;
                item.setChecked(true);
                break;
            case R.id.ghostColor_red:
                Settings.GhostImageFilterColor = Color.RED;
                item.setChecked(true);
                break;
            case R.id.menu_calibrationMode:
                Settings.CalibrationModeEnabled = !Settings.CalibrationModeEnabled;
                item.setChecked(Settings.CalibrationModeEnabled);
                Settings.GhostImageScaleFactor = 1.0;
                if(!Settings.CalibrationModeEnabled)
                {
                    startCamera();
                }else
                {
                    stopCamera();
                }
                prepareGhostImage();
                updateDistanceUI();
                return true;
            default:
                return false;
        }
        _ghostImageView.setColorFilter(Settings.GhostImageFilterColor);
        return true;

    }

    private void RefreshGhostPickerMenu()
    {
        if (_ghostList.getVisibility() == View.VISIBLE)
        {
            SetupGhostPickerMenu();
        }
    }

    private void ToggleGhostPickerMenu()
    {
        if (_ghostList.getVisibility() == View.VISIBLE)
        {
            _ghostList.setVisibility(View.INVISIBLE);
        }
        else
        {
            SetupGhostPickerMenu();
            _ghostList.setVisibility(View.VISIBLE);
        }

    }

    private void SetupGhostPickerMenu()
    {
        GhostAdapter adapter = new GhostAdapter(this, Ghost.GetAllGhosts());
        _ghostList.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        _ghostList.setLayoutManager(layoutManager);
        _ghostList.addItemDecoration(new DividerItemDecoration(_ghostList.getContext(),
                DividerItemDecoration.HORIZONTAL));
    }

    @Override
    public void ghostMenuItemClicked(int position)
    {
        Settings.CurrentGhostName = Ghost.GetAllGhosts()[position].Name;
        Settings.GhostImageScaleFactor = 1.0;

        prepareGhostImage();
        updateDistanceUI();
        ToggleGhostPickerMenu();
    }
    //endregion


    //region Camera Code
    private void startCamera()
    {
        //make sure there isn't another camera instance running before starting
        stopCamera();

        Display display = ((WindowManager) getSystemService(
                Context.WINDOW_SERVICE)).getDefaultDisplay();
        CameraPreview cameraPreview = new CameraPreview(_rootLayout, display, _cameraView,
                _ghostImageView);
        Preview preview = cameraPreview.startCamera();

        CameraX.bindToLifecycle(this, preview);
    }

    private void stopCamera()
    {
        CameraX.unbindAll();
    }
    //endregion

    //region Android Permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        //start camera when permissions have been granted otherwise exit app
        if (requestCode == REQUEST_CODE_PERMISSIONS)
        {
            if (allPermissionsGranted())
            {
                startCamera();
            }
            else
            {
                Toast.makeText(this, "Please enable Camera permissions",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted()
    {
        //check if req permissions have been granted
        for (String permission : REQUIRED_PERMISSIONS)
        {
            if (ContextCompat.checkSelfPermission(this,
                    permission) != PackageManager.PERMISSION_GRANTED)
            {
                return false;
            }
        }
        return true;
    }
    //endregion

    //region Touch Event Handlers
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        _scaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener
    {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector)
        {
            if (Settings.CalibrationModeEnabled)
            {
                return false;
            }

            Settings.GhostImageScaleFactor *= scaleGestureDetector.getScaleFactor();
            Settings.GhostImageScaleFactor = Math.max(0.001f,
                    Math.min(Settings.GhostImageScaleFactor, 10.0f));

            scaleGhostImage();
            updateDistanceUI();
            return true;
        }
    }

    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener
    {

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b)
        {
            float scaleFactor = (50 + (i - 50) / 2f) / 50f;

            if (seekBar == _verticalSeekBar)
            {
                Settings.SetYScaleFactor(scaleFactor);
            }
            else
            {
                Settings.SetXScaleFactor(scaleFactor);
            }
            scaleGhostImage();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }
    }
    //endregion

}
