package haus.stewart.rangefinder;

import android.graphics.Matrix;
import android.util.Rational;
import android.util.Size;
import android.view.Display;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;

public class CameraPreview
{
    // Camera settings to handle device orientation changes
    private int _cameraRotationDegrees;
    private Size _cameraViewBufferDimens = new Size(0, 0);
    private Size _previewBufferDimens = new Size(0, 0);

    private ViewGroup _parentViewGroup;
    private Display _display;
    private TextureView _cameraView;
    private ImageView _overlayView;

    public CameraPreview(ViewGroup parent, Display display, TextureView textureView, ImageView overlayView)
    {
        _parentViewGroup = parent;
        _display = display;
        _cameraView = textureView;
        _overlayView = overlayView;
    }


    //region Camera Preview and Rotation
    public Preview startCamera()
    {
        /* start preview */
        int aspRatioW = _cameraView.getWidth(); //get width of screen
        int aspRatioH = _cameraView.getHeight(); //get height
        Rational asp = new Rational(aspRatioW, aspRatioH); //aspect ratio
        Size screen = new Size(aspRatioW, aspRatioH); //size of the screen

        //config obj for preview/viewfinder thingy.
        PreviewConfig pConfig = new PreviewConfig.Builder().setTargetAspectRatio(
                asp).setTargetResolution(screen).build();
        Preview preview = new Preview(pConfig); //lets build it

        //to update the surface texture we have to destroy it first, then re-add it
        preview.setOnPreviewOutputUpdateListener(
                output ->
                {
                    _parentViewGroup.removeView(_cameraView);
                    _parentViewGroup.removeView(_overlayView);
                    _parentViewGroup.addView(_cameraView, 0);
                    _parentViewGroup.addView(_overlayView, 1);

                    _cameraView.setSurfaceTexture(output.getSurfaceTexture());
                    updateTransform(output.getRotationDegrees(), _cameraViewBufferDimens,
                            output.getTextureSize());

                });

        _cameraView.addOnLayoutChangeListener(new View.OnLayoutChangeListener()
        {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom, int i4, int i5, int i6, int i7)
            {
                Size newCameraViewBufferDimens = new Size(right - left, bottom - top);
                updateTransform(getRotationDegrees(), newCameraViewBufferDimens,
                        _previewBufferDimens);
            }
        });

        return preview;
    }

    private void updateTransform(int rotationDegrees, Size newCameraViewBufferDimens, Size newPreviewBufferDimens)
    {

        if (_cameraRotationDegrees == rotationDegrees &&
                _cameraViewBufferDimens == newCameraViewBufferDimens &&
                _previewBufferDimens == newPreviewBufferDimens)
        {
            // Nothing has changed, no need to transform output again
            return;
        }

        _cameraRotationDegrees = rotationDegrees;
        _cameraViewBufferDimens = newCameraViewBufferDimens;
        _previewBufferDimens = newPreviewBufferDimens;

        Matrix mx = new Matrix();

        float w = _cameraView.getMeasuredWidth();
        float h = _cameraView.getMeasuredHeight();

        if (w == 0 || h == 0 ||
                _cameraViewBufferDimens.getHeight() == 0 || _cameraViewBufferDimens.getWidth() == 0 ||
                _previewBufferDimens.getHeight() == 0 || _previewBufferDimens.getWidth() == 0)
        {
            // Invalid view finder dimens - wait for valid inputs before setting matrix
            return;
        }

        float centreX = w / 2f; //calc centre of the viewfinder
        float centreY = h / 2f;

        mx.postRotate((float) -_cameraRotationDegrees, centreX, centreY);

        boolean isNaturalPortrait = ((_cameraRotationDegrees == 0 || _cameraRotationDegrees == 180) &&
                w < h)
                || ((_cameraRotationDegrees == 90 || _cameraRotationDegrees == 270) &&
                w >= h);

        int previewBufferWidth;
        int previewBufferHeight;
        if (isNaturalPortrait)
        {
            previewBufferWidth = _previewBufferDimens.getHeight();
            previewBufferHeight = _previewBufferDimens.getWidth();
        }
        else
        {
            previewBufferWidth = _previewBufferDimens.getWidth();
            previewBufferHeight = _previewBufferDimens.getHeight();
        }

        float xScale = previewBufferWidth / (float) _cameraViewBufferDimens.getWidth();
        float yScale = previewBufferHeight / (float) _cameraViewBufferDimens.getHeight();

        int previewBufferRotatedWidth;
        int previewBufferRotatedHeight;
        if (_cameraRotationDegrees == 0 || _cameraRotationDegrees == 180)
        {
            previewBufferRotatedWidth = previewBufferWidth;
            previewBufferRotatedHeight = previewBufferHeight;
        }
        else
        {
            previewBufferRotatedWidth = previewBufferHeight;
            previewBufferRotatedHeight = previewBufferWidth;
        }

        // Scale the buffer so that it just covers the viewfinder.
        float scale = Math.max(
                _cameraViewBufferDimens.getWidth() / (float) previewBufferRotatedWidth,
                _cameraViewBufferDimens.getHeight() / (float) previewBufferRotatedHeight);
        xScale *= scale;
        yScale *= scale;

        // Scale input buffers to fill the view finder
        mx.preScale(xScale, yScale, centreX, centreY);

        _cameraView.setTransform(mx); //apply transformations to textureview
    }

    private int getRotationDegrees()
    {
        int rotationDgr;
        int rotation = _display.getRotation();

        switch (rotation)
        { //correct output to account for display rotation
            case Surface.ROTATION_0:
                rotationDgr = 0;
                break;
            case Surface.ROTATION_90:
                rotationDgr = 90;
                break;
            case Surface.ROTATION_180:
                rotationDgr = 180;
                break;
            case Surface.ROTATION_270:
                rotationDgr = 270;
                break;
            default:
                return 0;
        }
        return rotationDgr;
    }
    //endregion
}
