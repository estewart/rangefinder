package haus.stewart.rangefinder;

import java.text.DecimalFormat;

public class Units
{

    public static String FeetDoubleToString(double feetDouble)
    {
        return FeetDoubleToString(feetDouble, false);
    }

    public static String FeetDoubleToString(double feetDouble, boolean precision)
    {
        if (Settings.GetUseMetricUnits())
        {
            return FeetDoubleToMetricString(feetDouble, precision);
        }
        else
        {
            return FeetDoubleToFeetString(feetDouble, precision);
        }
    }

    public static String FeetDoubleToFeetString(double feetDouble, boolean precision)
    {
        if (feetDouble > 1)
        {
            int feet = (int) Math.floor(feetDouble);
            int inches = (int) ((feetDouble - feet) * 12);
            return feet + "ft " + inches + "in";
        }
        else
        {
            DecimalFormat df = precision ? new DecimalFormat("##.##") : new DecimalFormat("##.#");
            return df.format(feetDouble * 12) + "in";
        }
    }

    public static String FeetDoubleToMetricString(double feetDouble, boolean precision)
    {
        double metricValue = feetDouble * 0.3048;
        if (metricValue < 1)
        {
            int meters = (int) Math.floor(metricValue);
            double cm = ((metricValue - meters) * 100);
            DecimalFormat df = precision ? new DecimalFormat("##.##") : new DecimalFormat("##");
            return df.format(cm) + "cm";
        }
        else if (metricValue < 10)
        {
            DecimalFormat df = new DecimalFormat("##.##");
            return df.format(metricValue) + "m";
        }
        else
        {
            DecimalFormat df = new DecimalFormat("###.#");
            return df.format(metricValue) + "m";
        }
    }
}
