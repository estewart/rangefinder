package haus.stewart.rangefinder;

import android.content.Context;
import android.graphics.drawable.Drawable;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class Ghost implements Comparable<Ghost>
{

    public static ArrayList<Ghost> Ghosts;
    public String Name;
    public int ImgResourceId;
    public Drawable ImgResource;
    public double ActualHeightInFeet;
    public double ActualWidthInFeet;
    public double KnownDistanceInFeet;
    public double ScreenHeightAtDistanceInFeet;
    public double ScreenWidthAtDistanceInFeet;

    public static void LoadGhosts(Context context)
    {
        if (Ghosts == null)
        {
            XmlPullParserFactory parserFactory;
            try
            {
                parserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = parserFactory.newPullParser();
                InputStream is = context.getAssets().open("ghosts.xml");
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(is, null);

                processParsing(context, parser);

            }
            catch (XmlPullParserException e)
            {

            }
            catch (IOException e)
            {
            }
        }
    }

    public static Ghost GetReferenceGhost(Context context)
    {
        double one_inch_in_feet = .08333334;
        Ghost reference = new Ghost();
        reference.ActualHeightInFeet = one_inch_in_feet;
        reference.ActualWidthInFeet = one_inch_in_feet;
        reference.ScreenHeightAtDistanceInFeet = one_inch_in_feet;
        reference.ScreenWidthAtDistanceInFeet = one_inch_in_feet;
        reference.Name = "One Inch";
        reference.ImgResourceId = R.drawable.ic_inch;
        reference.ImgResource = context.getDrawable(reference.ImgResourceId);

        return reference;
    }

    public static Ghost[] GetAllGhosts()
    {
        if (Ghosts == null)
        {
            return null;
        }
        Collections.sort(Ghosts);
        Ghost[] allGhosts = new Ghost[Ghosts.size()];
        Ghosts.toArray(allGhosts);
        return allGhosts;
    }

    public static Ghost GetGhost(String name)
    {
        for (Ghost g : Ghosts)
        {
            if (g.Name.equals(name))
            {
                return g;
            }
        }
        return null;
    }

    private static void processParsing(Context context, XmlPullParser parser) throws IOException, XmlPullParserException
    {
        Ghosts = new ArrayList<Ghost>();
        int eventType = parser.getEventType();
        Ghost currentGhost = null;

        while (eventType != XmlPullParser.END_DOCUMENT)
        {
            String eltName = null;

            switch (eventType)
            {
                case XmlPullParser.START_TAG:
                    eltName = parser.getName();

                    if ("ghost".equals(eltName))
                    {
                        currentGhost = new Ghost();
                    }
                    else if (currentGhost != null)
                    {
                        switch (eltName)
                        {
                            case "name":
                                currentGhost.Name = parser.nextText();
                                Ghosts.add(currentGhost);
                                break;
                            case "imageResourceName":
                                currentGhost.ImgResourceId = context.getResources().getIdentifier(
                                        parser.nextText(), "drawable", context.getPackageName());
                                currentGhost.ImgResource = context.getDrawable(
                                        currentGhost.ImgResourceId);
                                break;
                            case "actualHeightInFeet":
                                currentGhost.ActualHeightInFeet = Double.parseDouble(
                                        parser.nextText());
                                break;
                            case "actualWidthInFeet":
                                currentGhost.ActualWidthInFeet = Double.parseDouble(
                                        parser.nextText());
                                break;
                            case "knownDistanceInFeet":
                                currentGhost.KnownDistanceInFeet = Double.parseDouble(
                                        parser.nextText());
                                break;
                            case "screenHeightAtDistanceInFeet":
                                currentGhost.ScreenHeightAtDistanceInFeet = Double.parseDouble(
                                        parser.nextText());
                                break;
                            case "screenWidthAtDistanceInFeet":
                                currentGhost.ScreenWidthAtDistanceInFeet = Double.parseDouble(
                                        parser.nextText());
                                break;
                        }

                    }
                    break;
            }

            eventType = parser.next();
        }
    }

    @Override
    public int compareTo(Ghost ghost)
    {
        return Double.compare(this.ActualHeightInFeet, ghost.ActualHeightInFeet);
    }
}
