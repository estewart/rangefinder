package haus.stewart.rangefinder;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

public class Settings
{
    // App Settings Identifiers
    private static final String SCALE_FACTOR = "SCALE_FACTOR";
    private static final String GHOST_IMAGE_NAME = "GHOST_IMAGE_NAME";
    private static final String CALIBRATION_MODE = "CALIBRATION_MODE";
    private static final String GHOST_COLOR = "GHOST_COLOR";
    private static final String SETTINGS_XML = "settings.xml";
    private static final String SETTINGS = "settings";
    private static final String USE_METRIC = "useMetric";
    private static final String X_SCALE_FACTOR = "xScaleFactor";
    private static final String Y_SCALE_FACTOR = "yScaleFactor";

    // Ephemeral settings (Short-term, while app is running)
    public static String CurrentGhostName;
    public static boolean CalibrationModeEnabled;
    public static double GhostImageScaleFactor;
    public static int GhostImageFilterColor;

    // Singleton instance
    private static Settings SettingsInstance;

    // Persisted settings (Long-term, on device storage)
    private boolean _useMetric;
    private float _xScaleFactor;
    private float _yScaleFactor;

    // Context (required to persist app settings to storage)
    private Context _context;

    private Settings(Context context)
    {
        _context = context;
        _useMetric = false;
        _xScaleFactor = 1f;
        _yScaleFactor = 1f;
    }

    public static void LoadEphemeralSettings(Bundle appSettings)
    {
        if (appSettings != null)
        {
            CurrentGhostName = appSettings.getString(GHOST_IMAGE_NAME, "Man");
            GhostImageScaleFactor = appSettings.getDouble(SCALE_FACTOR, 1.0);
            CalibrationModeEnabled = appSettings.getBoolean(CALIBRATION_MODE, false);
            GhostImageFilterColor = appSettings.getInt(GHOST_COLOR, Color.BLACK);
        }
        else
        {
            CurrentGhostName = "Man";
            GhostImageScaleFactor = 1.0;
            CalibrationModeEnabled = false;
            GhostImageFilterColor = Color.BLACK;
        }
    }

    public static void SaveEphemeralSettings(Bundle appSettings)
    {
        appSettings.putDouble(SCALE_FACTOR, GhostImageScaleFactor);
        appSettings.putString(GHOST_IMAGE_NAME, CurrentGhostName);
        appSettings.putBoolean(CALIBRATION_MODE, CalibrationModeEnabled);
        appSettings.putInt(GHOST_COLOR, GhostImageFilterColor);
    }

    public static boolean GetUseMetricUnits()
    {
        return SettingsInstance._useMetric;
    }

    public static float GetXScaleFactor()
    {
        return SettingsInstance._xScaleFactor;
    }

    public static float GetYScaleFactor()
    {
        return SettingsInstance._yScaleFactor;
    }

    public static void ToggleUseMetricUnits()
    {
        SettingsInstance._useMetric = !SettingsInstance._useMetric;
        SettingsInstance.SavePersistedSettings();
    }

    public static void SetXScaleFactor(float newScaleFactor)
    {
        SettingsInstance._xScaleFactor = newScaleFactor;
        SettingsInstance.SavePersistedSettings();
    }

    public static void SetYScaleFactor(float newScaleFactor)
    {
        SettingsInstance._yScaleFactor = newScaleFactor;
        SettingsInstance.SavePersistedSettings();
    }

    public static Settings LoadPersistedSettings(Context context)
    {
        if (SettingsInstance != null)
        {
            return SettingsInstance;
        }

        XmlPullParserFactory parserFactory;
        try
        {
            SettingsInstance = new Settings(context);

            parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();
            InputStream is = context.openFileInput(SETTINGS_XML);
            ;
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(is, null);

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                String eltName = null;

                switch (eventType)
                {
                    case XmlPullParser.START_TAG:
                        eltName = parser.getName();
                        switch (eltName)
                        {
                            case USE_METRIC:
                                SettingsInstance._useMetric = Boolean.parseBoolean(
                                        parser.nextText());
                                break;
                            case X_SCALE_FACTOR:
                                SettingsInstance._xScaleFactor = Float.parseFloat(
                                        parser.nextText());
                                break;
                            case Y_SCALE_FACTOR:
                                SettingsInstance._yScaleFactor = Float.parseFloat(
                                        parser.nextText());
                                break;
                        }
                        break;
                }

                eventType = parser.next();
            }

        }
        catch (XmlPullParserException e)
        {

        }
        catch (IOException e)
        {
        }
        return SettingsInstance;
    }

    public void SavePersistedSettings()
    {
        try
        {
            FileOutputStream fileos = _context.openFileOutput(SETTINGS_XML, Context.MODE_PRIVATE);
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, SETTINGS);
            xmlSerializer.startTag(null, USE_METRIC);
            xmlSerializer.text(Boolean.toString(_useMetric));
            xmlSerializer.endTag(null, USE_METRIC);
            xmlSerializer.startTag(null, X_SCALE_FACTOR);
            xmlSerializer.text(Float.toString(_xScaleFactor));
            xmlSerializer.endTag(null, X_SCALE_FACTOR);
            xmlSerializer.startTag(null, Y_SCALE_FACTOR);
            xmlSerializer.text(Float.toString(_yScaleFactor));
            xmlSerializer.endTag(null, Y_SCALE_FACTOR);
            xmlSerializer.endTag(null, SETTINGS);
            xmlSerializer.endDocument();
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();
        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
