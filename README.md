# Rangefinder

This is the repository for [Rangefinder](https://play.google.com/store/apps/details?id=haus.stewart.rangefinder), an android app on the Google Play Store.

This app uses [Stadiametric rangefinding](https://en.wikipedia.org/wiki/Stadiametric_rangefinding) to take an object of
a known height at a known distance, and use the principle of similar triangles to 
estimate the distance of a phone to objects of similar heights.

Instead of a fixed scale, this app projects silhouette images of various objects of
known heights at a known distance (for instance, a 5'8" tall man at 10ft) onto the
screen.  They can be scaled using a pinch motion on the phone and overlayed on
faraway objects to help a user estimate the distance of the object.

# Getting started in Developing Rangefinder

First thing is to get Android Studio installed on your machine, so you can do 
development using the Android IDE. Other IDE options are possible, but not directly
described or supported here. If you're using your own IDE, it should be fairly 
straightforward to convert these instructions to use with your preferred toolchain.

## Code
**MainActivity.java**, handles the UI components as well as managing the distance calculation.

**GhostAdapter.java** handles creating the scrolling menu for selecting ghost images.

**Ghost.java** handles loading in the resources from ghosts.xml and representing them in the app.

**Units.java** handles unit conversions and formatting.

**Settings.java** handles persistence of settings.

## Resources
The key resources for this app are the ghost images and their characteristics. There's a format defined for these in rangefinder/app/src/main/assets/ghosts.xml.  

An example entry:
```
    <ghost>
        <name>Man</name>
        <imageResourceName>ic_manoutline</imageResourceName>
        <actualHeightInFeet>5.74147</actualHeightInFeet>
        <actualWidthInFeet>1.94</actualWidthInFeet>
        <knownDistanceInFeet>10</knownDistanceInFeet>
        <screenHeightAtDistanceInFeet>.1575</screenHeightAtDistanceInFeet>
        <screenWidthAtDistanceInFeet>.0532</screenWidthAtDistanceInFeet>
    </ghost>
```

Additionally, each ghost needs to define an image resource.  Ideally this would be an SVG or a high-resolution image so it doesn't look pixelated.  The resource name must be referenced in the xml entry.

### Adding New Ghost Images
Right now, only developers who are willing to clone or fork the repo are going to have much success adding new ghost images.  Eventually it may be possible to add new images through the app.

1. Create or find an image asset that's a good representation of the proportions of the object you want to create.  Crop the image as close to the outline of the image as possible.
2. Add the image asset to the project.
3. Add a ghost entry to ghosts.xml referencing the image asset.  Make sure to add good values for actualHeightInFeet and actualWidthInFeet.  Don't worry about getting the on-screen dimensions correct just yet.  *It is possible to get the correct dimensions for your object by doing the math, but I won't describe that here*
4. Rebuild the app and deploy it on your phone.
5. Select your ghost image from the menu.
6. At a known distance from your object, scale the ghost image so it's overlaid with the real object.  If you don't have access to the real object, use something that is the same height and/or width. **IMPORTANT:** Make sure the whole ghost image fits on the screen.
7. Measure the height and width of the ghost image on the screen.
8. Update the ghost entry in ghosts.xml to reflect the measurements you took on screen and the known distance at which you took them.


# Reference links

This repository is set up to run CI/CD with [fastlane](https://fastlane.tools/).
I have found the following links to be very helpful:

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Blog post: Android publishing with GitLab and fastlane](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/)

As of now, this setup is working.  In the future it may need tweaking.

Some of the initial CameraX code copied from: [https://github.com/thunderedge/CameraX](https://github.com/thunderedge/CameraX)
